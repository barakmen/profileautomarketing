#!/usr/bin/python3.7
import sys
import datetime
import time
from bs4 import BeautifulSoup
import requests
sys.stderr.write("1111111111111\n")

minute = 60
hour = minute*60
three_hours_plus_one = hour*3 + 1


headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
}

login_data = {
    'name': '<username>',
    'pass': '<password>',
    'form_id': 'user_login',
    'op': 'כניסה'
}

sys.stderr.write("2222222222222222222222\n")


def main():
    rest_first_run = 0*minute
    time.sleep(rest_first_run)

    while 1:
        sys.stderr.write("start work:  " + str(datetime.datetime.now()) + "\n")
        with requests.Session() as s:
            url = 'https://www.limudnaim.co.il/user?destination=%2Fuser%2F87285'
            r = s.get(url, headers=headers)
            soup = BeautifulSoup(r.content, 'html5lib')
            # print(str(soup))
            login_data['form_build_id'] = soup.find(
                'input', attrs={'name': 'form_build_id'})['value']
            r = s.post(url, data=login_data, headers=headers)
            # print(r.content)

            updateProfileURL = 'https://www.limudnaim.co.il/node/184190/update-modified'
            r = s.get(updateProfileURL, headers=headers)
            # print(str(r.content))

        sys.stderr.write("go to sleep..." +
                         str(datetime.datetime.now()) + "\n")
        time.sleep(three_hours_plus_one)


sys.stderr.write("3333333333333333\n")
sys.stderr.write("script start!\n")
main()
